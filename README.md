# Note

A simple Java library for working with formatted text. Parse HTML or Markdown into a simple graph structure, create that graph via an API, and then emit in HTML, PDF or a variety of other formats. Extensible and driven by plugins.
